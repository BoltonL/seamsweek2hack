# SEAMSweek2hack

## Blood Usage and Demand in South Africa  

Retrospective analysis of operational data collected from red blood cell product requests of the South African National Blood Service (SANBS) and Western Cape Blood Service (WCBS) between 1 January 2014 and 31 March 2019.  
  
*Aim of project*    
Determine any patterns in demographics and disease spectrums (especially stratified by health sector) to determine possible clinical drivers of usage and demand.  
  
*Process diagram*  
**Input**: Records of red cell blood product requests of SANBS over 6 years between 1 January 2014 and 31 March 2019 (csv format, product-level per service date)  
**Data cleaning**: Re-factor variables and assign variable classes  
**Pre-processing** Linkage between hospital and blood bank zone and imputation of missing issued counts  
**Intermediary output**: Pre-processed data set  
**Analysis**: Identify patterns in patient demographics and patterns in product usage stratified by public or private health sector  
**Output**: Informative visualisations and descriptive statistics of usage  

