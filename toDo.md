#To do list:  
  
## Minimal goal by Friday:  
  1. Replace copy and paste code with functions in imputation script **DONE**  
  2. Save generated imputed dataset as .RDS **DONE**  
  3. Define and implement tests for imputation method **DONE**  
  4. Generated detailed documentation on process and functions **DONE**  
  
## Ideal goals by Friday:  
Minimal goals plus...  
  1. Re-design diagnosis categorisation step in analysis script. More specifically:  
    1.1 Create sample diagnosis dictionary  
    1.2 Define function to implement rules specified in (1.1)  
    1.3 Test on dummy data  
    1.4 Populate dictionary  
    1.5 Add in automation step that expands dictionary (begin with dummy function)
    

